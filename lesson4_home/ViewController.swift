//
//  ViewController.swift
//  lesson4_home
//
//  Created by 1 on 28.08.2018.
//  Copyright © 2018 web-academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

       // task_1("Tatyana")
        
        task_2("Богдановна")
    }

    func task_2(_ fname: NSString) {
        if fname.hasSuffix("на") {
            print("\(fname) имеет женский суфикс. и заканчивается на \"на\"")
        } else {
            if fname.hasSuffix("ич") {
                print("\(fname) имеет мужский суфикс. и заканчивается на \"ич\"")
            } else  {
                print("Отчество не распознано")
            }
        }
    }
    
    func task_1(_ name: NSString) {
        print("Name \"\(name)\". length \(name.length)")
    }
    
    

}

